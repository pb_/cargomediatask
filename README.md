Cargo Media Task
=======================

Introduction
------------
This is an application that use the ZF2 MVC layer and module systems.
It contains an very simple API that allows for three basic operations to be executed for a certain person:

Direct friends: Return those people who are directly connected to the chosen person.

Friends of friends: Return those who are two steps away, but not directly connected to the chosen person.

Suggested friends: Return people in the group who know 2 or more direct friends of the chosen person, but are not directly connected to her.

Installation
------------

Using Composer 
----------------------------

Clone the repository and manually invoke `composer` using the `composer.phar`:

    cd my/project/dir
    git clone git@bitbucket.org:pb_/cargomediatask.git
    cd CargomediaTask
    php composer.phar self-update
    php composer.phar install

Database
----------------    
Create database:
	
	cd vendor/doctrine/doctrine-module/bin
	php doctrine-module.php orm:schema-tool:update --force

Enter url:

	/import
	
Or use sql file:

	cd data/db.sql


How to use
----------------
Direct friends: 
	
	/api/user/direct-friends/[:userId]

Friends of friends: 
	
	/api/user/friends-of-friends/[:userId]

Suggested friends: 
	
	/api/user/suggested-friends/[:userId]


How to test
----------------
Application contains a PHPUnit just go to:
	
	cd module/Application/test/

and run

	php phpunit.phar  
	

Demo
----------------
Direct friends: 

	http://cargomedia.eu1.frbit.net/api/user/direct-friends/1
	
Friends of friends: 

	http://cargomedia.eu1.frbit.net/api/user/friends-of-friends/2
	
Suggested friends: 

	http://cargomedia.eu1.frbit.net/api/user/suggested-friends/11