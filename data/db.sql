-- Adminer 4.0.2 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = '+00:00';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `friends`;
CREATE TABLE `friends` (
  `user_id` int(11) NOT NULL,
  `friend_user_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`friend_user_id`),
  KEY `IDX_21EE7069A76ED395` (`user_id`),
  KEY `IDX_21EE706993D1119E` (`friend_user_id`),
  CONSTRAINT `FK_21EE706993D1119E` FOREIGN KEY (`friend_user_id`) REFERENCES `User` (`id`),
  CONSTRAINT `FK_21EE7069A76ED395` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `friends` (`user_id`, `friend_user_id`) VALUES
(1,	2),
(2,	1),
(2,	3),
(3,	2),
(3,	4),
(3,	5),
(3,	7),
(4,	3),
(5,	3),
(5,	6),
(5,	7),
(5,	10),
(5,	11),
(6,	5),
(7,	3),
(7,	5),
(7,	8),
(7,	12),
(7,	20),
(8,	7),
(9,	12),
(10,	5),
(10,	11),
(11,	5),
(11,	10),
(11,	19),
(11,	20),
(12,	7),
(12,	9),
(12,	13),
(12,	20),
(13,	12),
(13,	14),
(13,	20),
(14,	13),
(14,	15),
(15,	14),
(16,	18),
(16,	20),
(17,	18),
(17,	20),
(18,	17),
(19,	11),
(19,	20),
(20,	7),
(20,	11),
(20,	12),
(20,	13),
(20,	16),
(20,	17),
(20,	19);

DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `age` smallint(6) DEFAULT NULL,
  `gender` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `User` (`id`, `firstName`, `surname`, `age`, `gender`, `status`) VALUES
(1,	'Paul',	'Crowe',	28,	'male',	1),
(2,	'Rob',	'Fitz',	23,	'male',	1),
(3,	'Ben',	'O\'Carolan',	NULL,	'male',	1),
(4,	'Victor',	'',	28,	'male',	1),
(5,	'Peter',	'Mac',	29,	'male',	1),
(6,	'John',	'Barry',	18,	'male',	1),
(7,	'Sarah',	'Lane',	30,	'female',	1),
(8,	'Susan',	'Downe',	28,	'female',	1),
(9,	'Jack',	'Stam',	28,	'male',	1),
(10,	'Amy',	'Lane',	24,	'female',	1),
(11,	'Sandra',	'Phelan',	28,	'female',	1),
(12,	'Laura',	'Murphy',	33,	'female',	1),
(13,	'Lisa',	'Daly',	28,	'female',	1),
(14,	'Mark',	'Johnson',	28,	'male',	1),
(15,	'Seamus',	'Crowe',	24,	'male',	1),
(16,	'Daren',	'Slater',	28,	'male',	1),
(17,	'Dara',	'Zoltan',	48,	'male',	1),
(18,	'Marie',	'D',	28,	'female',	1),
(19,	'Catriona',	'Long',	28,	'female',	1),
(20,	'Katy',	'Couch',	28,	'female',	1);

-- 2014-01-29 15:17:27
