<?php
/**
*
* Test for Direct Friends, Friends of Friends, SuggestedFriends
*
*/
namespace ApplicationTest\Controller;

use ApplicationTest\Bootstrap;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use Application\Controller\ApiController;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use PHPUnit_Framework_TestCase;
use Zend\View\Model\JsonModel; 


class ApiControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $controller;
    protected $request;
    protected $response;
    protected $routeMatch;
    protected $event;

    protected function setUp()
    {
        $serviceManager = Bootstrap::getServiceManager();
        $this->controller = new ApiController();
        $this->request    = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'index'));
        $this->event      = new MvcEvent();
        $config = $serviceManager->get('Config');
        $routerConfig = isset($config['router']) ? $config['router'] : array();
        $router = HttpRouter::factory($routerConfig);

        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($serviceManager);
    }
    
    public function testApiActionCanBeAccessed()
	{
		$this->routeMatch->setParam('action', 'direct-friends');

		$result   = $this->controller->dispatch($this->request);
		$response = $this->controller->getResponse();

		$this->assertEquals(200, $response->getStatusCode());
	}
	
	public function testApiDirectFriends()
	{
		$this->routeMatch->setParam('id', 1);
		
		$directFriends = $this->controller->directFriendsAction();
		$friends = \Zend\Json\Json::decode('[{"id":2,"firstName":"Rob","surname":"Fitz","age":23,"gender":"male"}]',  \Zend\Json\Json::TYPE_ARRAY);
		$expected = new JsonModel(array('status' => true, 'error' => false, 'friends' => $friends ));
		
		$this->assertEquals($expected, $directFriends);
	}
	
	public function testFriendsOfFriendsAction()
	{
		$this->routeMatch->setParam('id', 2);
		
		$directFriends = $this->controller->friendsOfFriendsAction();
		$friends = \Zend\Json\Json::decode('[{"id":4,"firstName":"Victor","surname":"","age":28,"gender":"male"},{"id":5,"firstName":"Peter","surname":"Mac","age":29,"gender":"male"},{"id":7,"firstName":"Sarah","surname":"Lane","age":30,"gender":"female"}]',  \Zend\Json\Json::TYPE_ARRAY);
		$expected = new JsonModel(array('status' => true, 'error' => false, 'friends' => $friends ));
		
		$this->assertEquals($expected, $directFriends);
	}
	
	public function testSugestedFriendsAction()
	{
		$this->routeMatch->setParam('id', 11);
		
		$directFriends = $this->controller->suggestedFriendsAction();
		$friends = \Zend\Json\Json::decode('[{"id":7,"firstName":"Sarah","surname":"Lane","age":30,"gender":"female"}]',  \Zend\Json\Json::TYPE_ARRAY);
		$expected = new JsonModel(array('status' => true, 'error' => false, 'friends' => $friends ));
		
		$this->assertEquals($expected, $directFriends);
	}
}
