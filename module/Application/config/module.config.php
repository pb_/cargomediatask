<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'import' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/import',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Import',
                        'action'     => 'index',
                    ),
                ),
            ),
            'api-direct-friends' => array(
				'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
					'route' => '/api/user/direct-friends[/:id]',
					'constraints' => array(
						'id' => '[0-9]+',
					),                    
					'defaults' => array(
						'controller' => 'Application\Controller\Api',
						'action'     => 'direct-friends',
                    ),
                ),
            ), 
            'api-friends-of-friends' => array(
				'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
					'route' => '/api/user/friends-of-friends[/:id]',
					'constraints' => array(
						'id' => '[0-9]+',
					),                    
					'defaults' => array(
						'controller' => 'Application\Controller\Api',
						'action'     => 'friends-of-friends',
                    ),
                ),
            ),
            'api-suggested-friends' => array(
				'type' => 'Zend\Mvc\Router\Http\Segment',
                'options' => array(
					'route' => '/api/user/suggested-friends[/:id]',
					'constraints' => array(
						'id' => '[0-9]+',
					),                    
					'defaults' => array(
						'controller' => 'Application\Controller\Api',
						'action'     => 'suggested-friends',
                    ),
                ),
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'application' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/application',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' 	=> 'Application\Controller\IndexController',
            'Application\Controller\Import' => 'Application\Controller\ImportController',
            'Application\Controller\Api' 	=> 'Application\Controller\ApiController'
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'doctrine' => array(
	  'driver' => array(
		'application_entities' => array(
		  'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
		  'cache' => 'array',
		  'paths' => array(__DIR__ . '/../src/Application/Entity')
		),

		'orm_default' => array(
		  'drivers' => array(
			'Application\Entity' => 'application_entities'
		  )
	))),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
