<?php
/**
 * API Controller
 *
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel; 


class ApiController extends AbstractActionController
{
	/**
	*
	* Get direct friends
	*
	* @return JSON
	*/
    public function directFriendsAction()
    {
        $status = true;
    	$error  = false;
    	$friends = array();

    	$userId = $this->params()->fromRoute('id', 1);
    	
    	$objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
		
		$user = $objectManager->getRepository('\Application\Entity\User')->findOneById($userId);
		
		if(!$user){
		    $status = false;
    		$error  = 'Missing user';
		}else{
			foreach($user->getMyFriends() as $friend){
				$friends[] = $friend->toArray();
			}
		}
        return new JsonModel( array('status' => $status, 'error' => $error, 'friends' => $friends ) );
    }
    
    /**
	*
	* Get friends of friends
	*
	* @return JSON
	*/
    public function friendsOfFriendsAction()
    {
        $status = true;
    	$error  = false;
    	$friends = array();
    	
    	$userId = $this->params()->fromRoute('id', 1);
    	
    	$objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
		
		$user = $objectManager->getRepository('\Application\Entity\User')->findOneById($userId);
    	
    	if(!$user){
		    $status = false;
    		$error  = 'Missing user';
		}else{
			//direct friends and chosen user - to exclude
			$directFriends = array($user->getId() => $user->getId()); 
			//iterate  friends
			foreach($user->getMyFriends() as $friend){	
				//iterate friends of friends
				foreach( $friend->getMyFriends() as $friendFriends){ 							
					$friends[$friendFriends->getId()] = $friendFriends->toArray();
				}
				$directFriends[ $friend->getId()] = $friend->getId(); //get direct friends to exclude
			}
			//exclude friends and chosen user
			$friends = array_diff_key($friends, $directFriends);
			//sort friends
			ksort($friends);	
		}
    	return new JsonModel( array('status' => $status, 'error' => $error, 'friends' => array_values($friends) ) );
    }
    
    /**
	*
	* Get suggested friends
	*
	* @return JSON
	*/
    public function suggestedFriendsAction()
    {
        $status = true;
    	$error  = false;
    	$friends = array();
    	
    	$userId = $this->params()->fromRoute('id', 1);
    	
    	$objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
		
		$user = $objectManager->getRepository('\Application\Entity\User')->findOneById($userId);
    	
    	if(!$user){
		    $status = false;
    		$error  = 'Missing user';
		}else{
			//direct friends and chosen user - to exclude
			$directFriends = array($user->getId()); 
			$friendsOfFriends = array();
			$allFriends = array();
			$suggestedFriends = array();
			
			//iterate  friends
			foreach($user->getMyFriends() as $friend){	
				$directFriends[] = $friend->getId();
				//iterate friends of friends
				foreach( $friend->getMyFriends() as $friendFriends){ 
					//id friends of friends
					$friendsOfFriends[] =  $friendFriends->getId();
					//create array of all friends to reduce db queries
					$allFriends[$friendFriends->getId()] = $friendFriends;
				}
			}
			//exclude chosen user and direct friends
			$friendsOfFriends = array_diff($friendsOfFriends, $directFriends);
			
			//$friendsOfFriends = array_values($friendsOfFriends);
			//sort($friendsOfFriends);
			
			//count chosen user connection (2 or more)
			$suggestedFriendsIds = array_count_values($friendsOfFriends);
			
			//chose sugested friends 
			foreach($suggestedFriendsIds as $SFuserId => $countSuggestedFriend){
				//only for 2 or more connections
				if($countSuggestedFriend >= 2){
					$suggestedFriends[] = $allFriends[$SFuserId]->toArray();
				}
			}
		}
		//if suffested friends dont exist return error
		if(count($suggestedFriends) <= 0 ){
			$error = true;
		}
    	return new JsonModel( array('status' => $status, 'error' => $error, 'friends' => $suggestedFriends) );    
    }   
}
