<?php
/**
 * ImportController - import data form Cargo Media
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel; 


class ImportController extends AbstractActionController
{
	/**
	* Import data to DB
	*
	* @return JSON
	*/
    public function indexAction()
    {
    	$status = true;
    	$error  = false;
    	
    	//get data from remote server
    	$encodedValue = file_get_contents('http://www.cargomedia.ch/task/social-graph/data.json');

    	//convert data to php native
    	$data = \Zend\Json\Json::decode($encodedValue, \Zend\Json\Json::TYPE_OBJECT);

		//put data to DB
    	$objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
		
		//add users
		foreach($data as $k => $v){
			//add if user no exist
			if(	!$objectManager->getRepository('\Application\Entity\User')->findOneById($v->id)){ 
				$user = new \Application\Entity\User();
				$user->setFirstName($v->firstName);
				$user->setSurname($v->surname);
				$user->setAge($v->age);
				$user->setGender($v->gender);
				$user->setStatus(1);
				$objectManager->persist($user);
			}
		}
		$objectManager->flush();

		//add friends
		foreach($data as $k => $v){
			$user = $objectManager->getRepository('\Application\Entity\User')->findOneById($v->id);
			foreach($v->friends as $friend){ //iterate through friends from data
				$userFriend = $objectManager->getRepository('\Application\Entity\User')->findOneById($friend); 
				$user->addMyFriend($userFriend);
			}
			$objectManager->persist($user);			
		}
		
		try{
			$objectManager->flush();
		}
		catch(\Exception $e){
		}
		
        return new JsonModel( array('status' => $status, 'error' => $error) );
    }
}
